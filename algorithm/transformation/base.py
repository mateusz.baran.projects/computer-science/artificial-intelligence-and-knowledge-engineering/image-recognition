import numpy as np


class Transformation:
    def get_model(self, data):
        raise NotImplementedError

    def _result_vector(self, vector):
        raise NotImplementedError

    def _model(self, matrix, vector):
        try:
            inv_matrix = np.linalg.inv(matrix)
            multi_res = inv_matrix @ vector
            return self._result_vector(multi_res.flatten())
        except np.linalg.LinAlgError:
            return

    def min_points(self):
        raise NotImplementedError
