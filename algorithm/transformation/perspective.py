import numpy as np

from algorithm.transformation.base import Transformation


class PerspectiveTransformation(Transformation):
    def get_model(self, data):
        matrix = np.array([
            [data[0][0][0], data[0][0][1], 1.0, 0.0, 0.0, 0.0, -data[1][0][0] * data[0][0][0], -data[1][0][0] * data[0][0][1]],
            [data[0][1][0], data[0][1][1], 1.0, 0.0, 0.0, 0.0, -data[1][1][0] * data[0][1][0], -data[1][1][0] * data[0][1][1]],
            [data[0][2][0], data[0][2][1], 1.0, 0.0, 0.0, 0.0, -data[1][2][0] * data[0][2][0], -data[1][2][0] * data[0][2][1]],
            [data[0][3][0], data[0][3][1], 1.0, 0.0, 0.0, 0.0, -data[1][3][0] * data[0][3][0], -data[1][3][0] * data[0][3][1]],
            [0.0, 0.0, 0.0, data[0][0][0], data[0][0][1], 1.0, -data[1][0][1] * data[0][0][0], -data[1][0][1] * data[0][0][1]],
            [0.0, 0.0, 0.0, data[0][1][0], data[0][1][1], 1.0, -data[1][1][1] * data[0][1][0], -data[1][1][1] * data[0][1][1]],
            [0.0, 0.0, 0.0, data[0][2][0], data[0][2][1], 1.0, -data[1][2][1] * data[0][2][0], -data[1][2][1] * data[0][2][1]],
            [0.0, 0.0, 0.0, data[0][3][0], data[0][3][1], 1.0, -data[1][3][1] * data[0][3][0], -data[1][3][1] * data[0][3][1]]
        ])

        vector = np.array([
            data[1][0][0],
            data[1][1][0],
            data[1][2][0],
            data[1][3][0],
            data[1][0][1],
            data[1][1][1],
            data[1][2][1],
            data[1][3][1],
        ])

        return self._model(matrix, vector.T)

    def _result_vector(self, vector):
        return np.array([
            [vector[0], vector[1], vector[2]],
            [vector[3], vector[4], vector[5]],
            [vector[6], vector[7], 1.0]
        ])

    def min_points(self):
        return 4
