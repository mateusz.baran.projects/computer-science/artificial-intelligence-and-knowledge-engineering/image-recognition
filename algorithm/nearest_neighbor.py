import os
import pickle as pkl

import numpy as np

from model.image import Image
from model.image_compare import ImageCompare


class NearestNeighbor:
    CACHE_PATH = 'feature_distance.cache'

    def __init__(self, img_comp):
        self.img_comp = img_comp

    def _feature_distance(self):
        cache = pkl.load(open(self.CACHE_PATH, 'rb')) if os.path.exists(self.CACHE_PATH) else {}

        name = f'{self.img_comp.left.path}&&{self.img_comp.right.path}'
        if name in cache:
            return cache[name]

        matrix = []
        for f1 in self.img_comp.left.features:
            vector = []
            for f2 in self.img_comp.right.features:
                vector.append(np.linalg.norm(f1 - f2))
            matrix.append(vector)

        matrix = np.asarray(matrix)

        cache[name] = matrix

        with open(self.CACHE_PATH, 'wb') as file:
            pkl.dump(cache, file)

        return matrix

    @staticmethod
    def _coords_distance(coords):
        matrix = []
        for x1, y1 in coords:
            vector = []
            for x2, y2 in coords:
                vector.append(((x1 - x2) ** 2 + (y1 - y2) ** 2) ** .5)
            matrix.append(vector)

        return np.asarray(matrix)

    def _key_points(self):
        distance = self._feature_distance()
        dist_left_to_right = np.argmin(distance, axis=1)
        dist_right_to_left = np.argmin(distance, axis=0)

        points = []
        for li, ri in enumerate(dist_left_to_right):
            if dist_right_to_left[ri] == li:
                points.append((li, ri))

        return points

    def set_key_points(self):
        self.img_comp.pairs = self._key_points()

    def _consistent_key_points(self, size, threshold):
        key_points = np.array(self._key_points())
        coords1 = self.img_comp.left.coords[key_points[:, 0]]
        coords2 = self.img_comp.right.coords[key_points[:, 1]]

        dist1 = self._coords_distance(coords1)
        dist2 = self._coords_distance(coords2)

        neighbors1 = np.argsort(dist1, axis=1, kind='mergesort')[:, 1:size + 1]
        neighbors2 = np.argsort(dist2, axis=1, kind='mergesort')[:, 1:size + 1]

        consistent_key_points = []
        for i, key_point in enumerate(key_points):
            neighbors = np.intersect1d(neighbors1[i], neighbors2[i])

            if len(neighbors) / size >= threshold:
                consistent_key_points.append(key_point)

        return consistent_key_points

    def set_consistent_key_points(self, size, threshold):
        self.img_comp.pairs = self._consistent_key_points(size, threshold)

    def set_pairs(self, size, threshold):
        self.img_comp.pairs_filtered = self._consistent_key_points(size, threshold)


if __name__ == '__main__':
    file_name = 'wallet'
    img1 = Image(f'{file_name}1.png')
    img2 = Image(f'{file_name}2.png')

    ic = ImageCompare(img1, img2)

    NN = NearestNeighbor(ic)
    NN.set_key_points()
    NN.set_pairs(100, .5)

    ic.show()
