import numpy as np


class IterationHeuristic:
    def __init__(self, p=.6, w=.2):
        self.p = p
        self.w = w

    def nb_iters(self, iters, trans_min_points):
        numerator = np.log(1 - self.p)
        denominator = np.log(1 - np.power(self.w, trans_min_points))
        estimated_iter = int(numerator / (denominator + 1e-10))

        return np.minimum(iters, estimated_iter) if estimated_iter > 0 else iters
