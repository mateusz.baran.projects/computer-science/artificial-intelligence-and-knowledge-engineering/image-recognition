from random import sample

import numpy as np

from algorithm.heuristics.sample import SampleHeuristic


class DistanceHeuristic(SampleHeuristic):
    def __init__(self, r_min, r_max):
        self.r = r_min ** 2
        self.R = r_max ** 2

    def pair_sample(self, sample_size, img_comp, min_points):
        while True:
            pair = np.asarray(sample(img_comp.pairs, sample_size))

            if self.check_all_combination(self.calc_dist, pair[:min_points]):
                return pair

    @staticmethod
    def check_all_combination(func, data):
        for i in range(len(data) - 1):
            for j in range(i + 1, len(data)):
                if not func(data[i], data[j]):
                    return False
        return True

    def calc_dist(self, p1, p2):
        return self.r < (p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2 < self.R
