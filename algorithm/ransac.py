from functools import partial
from multiprocessing import Pool
from random import sample

import numpy as np
from tqdm import trange

from algorithm.heuristics.distance import DistanceHeuristic
from algorithm.heuristics.iteration import IterationHeuristic
from algorithm.heuristics.sample import SampleHeuristic
from algorithm.nearest_neighbor import NearestNeighbor
from algorithm.transformation.affine import AffineTransformation
from algorithm.transformation.perspective import PerspectiveTransformation
from model.image import Image
from model.image_compare import ImageCompare


class RANSAC:
    def __init__(self, img_comp, transformation, iters=500, max_error=10, sample_size=50,
                 iter_heuristic=None, sample_heuristic=None):
        self.pool = Pool()
        self.transformation = transformation

        self.img_comp = img_comp
        self.iters = iters
        self.max_error = max_error

        self.iter_heuristic = iter_heuristic
        self.sample_heuristic = sample_heuristic

        self.pairs = np.array(self.img_comp.pairs)

        self.data = []
        for left, right in zip(self.img_comp.left.coords[self.pairs[:, 0]],
                               self.img_comp.right.coords[self.pairs[:, 1]]):
            self.data.append([left, right])
        self.data = np.asarray(self.data)

        self.sample_size = np.clip(sample_size, 0, len(self.pairs))

    def choose_sample(self):
        pairs_sample = None

        if isinstance(self.sample_heuristic, SampleHeuristic):
            pairs_sample = self.sample_heuristic.pair_sample(self.sample_size,
                                                             self.img_comp,
                                                             self.transformation.min_points())

        if pairs_sample is None:
            pairs_sample = sample(self.img_comp.pairs, self.sample_size)

        pairs_sample = np.asarray(pairs_sample)
        coords = (self.img_comp.left.coords[pairs_sample[:, 0]],
                  self.img_comp.right.coords[pairs_sample[:, 1]])
        return coords

    def calculate_model(self, s):
        return self.transformation.get_model(s)

    @staticmethod
    def model_error(data, model):
        x, y = data[0]

        sec_matrix = np.array([x, y, 1.], dtype=np.float32)
        result = model @ sec_matrix.T

        t = result[2]
        u, v = result[0] / t, result[1] / t

        real_u, real_v = data[1]

        return np.sqrt(np.square(u - real_u) + np.square(v - real_v))

    def evaluate_model(self):
        best_model = None
        best_score = 0
        best_correct_pairs = []

        for _ in trange(self.nb_iters()):
            model = None

            while model is None:
                s = self.choose_sample()
                model = self.calculate_model(s)

            model_error = partial(self.model_error, model=model)
            errors = np.array(self.pool.map(model_error, self.data))
            correct_pairs = errors < self.max_error
            score = np.sum(correct_pairs)

            if score > best_score:
                best_model = model
                best_score = score
                best_correct_pairs = self.pairs[correct_pairs]

        return best_model, best_score, best_correct_pairs

    def nb_iters(self):
        if isinstance(self.iter_heuristic, IterationHeuristic):
            return self.iter_heuristic.nb_iters(self.iters, self.transformation.min_points())

        return self.iters

    def set_pairs(self):
        _, _, correct_pairs = self.evaluate_model()
        self.img_comp.pairs_filtered = correct_pairs


if __name__ == '__main__':
    file_name = 'mug'
    img1 = Image(f'{file_name}1.png')
    img2 = Image(f'{file_name}2.png')

    ic = ImageCompare(img1, img2)

    NN = NearestNeighbor(ic)
    NN.set_key_points()
    # NN.set_consistent_key_points(100, .6)

    # trans = AffineTransformation()
    trans = PerspectiveTransformation()

    iter_h = IterationHeuristic()
    dist_h = DistanceHeuristic(8, 240)

    ransac = RANSAC(ic, trans, iters=1000, iter_heuristic=iter_h, sample_heuristic=None)
    ransac.set_pairs()

    ic.show()
