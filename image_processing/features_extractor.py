import os

from settings import DATASET_PATH

path_descriptor = './extract_features_64bit.ln'

for _, _, files in os.walk(DATASET_PATH):
    for file in files:
        if (file.endswith('.png')
                and not file.endswith('sift.png')
                and f'{file}.haraff.sift' not in files):
            file = f'{DATASET_PATH}/{file}'
            print(f'\n file: {file}')
            os.system(f'{path_descriptor} -haraff -sift -i {file} -DE')
