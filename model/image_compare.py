import cv2
import numpy as np

from model.image import Image


class ImageCompare:
    LINE_PRIMARY_COLOR = (255, 0, 0)
    LINE_SECONDARY_COLOR = (0, 255, 255)
    LINE_THICKNESS = 1

    def __init__(self, left_image, right_image):
        self.left = left_image
        self.right = right_image

        assert self.left.shape == self.right.shape

        self.shape = self.left.shape
        self.canvas = np.concatenate((self.left.image, self.right.image), axis=1)
        self.pairs = []
        self.pairs_filtered = []

    def show(self):
        self.draw_points()
        while cv2.waitKey(100) != 27:
            cv2.imshow('ImageCompare', self.canvas)

    def draw_points(self):
        for p1, p2 in self.pairs:
            x1, y1 = self.left.coords[p1]
            x2, y2 = self.right.coords[p2]
            x2 += self.left.shape[1]
            cv2.line(self.canvas, (int(x1), int(y1)), (int(x2), int(y2)),
                     self.LINE_PRIMARY_COLOR,
                     self.LINE_THICKNESS)

        for p1, p2 in self.pairs_filtered:
            x1, y1 = self.left.coords[p1]
            x2, y2 = self.right.coords[p2]
            x2 += self.left.shape[1]
            cv2.line(self.canvas, (int(x1), int(y1)), (int(x2), int(y2)),
                     self.LINE_SECONDARY_COLOR,
                     self.LINE_THICKNESS)


if __name__ == '__main__':
    img1 = Image('muza1.png')
    img2 = Image('muza2.png')

    img_comp = ImageCompare(img1, img2)
    img_comp.pairs.append((10, 100))
    img_comp.pairs.append((211, 12))
    img_comp.pairs.append((664, 231))

    img_comp.show()
