import cv2
import numpy as np

from settings import DATASET_PATH


class Image:
    def __init__(self, name):
        self.path = f'{DATASET_PATH}/{name}'
        self._load()

    def _load(self):
        self.image = cv2.imread(self.path)
        self.shape = self.image.shape
        with open(self.path + '.haraff.sift') as file:
            lines = []
            for line in file.readlines()[2:]:
                lines.append(line.split())
            lines = np.asarray(lines, dtype=np.float32)
            self.coords = lines[:, 0:2]
            self.params = lines[:, 2:5]
            self.features = lines[:, 5:]
